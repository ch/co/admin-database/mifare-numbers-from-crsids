FROM python:3.10
EXPOSE 8000

# set up project light files
RUN mkdir -p /var/www/html
WORKDIR /var/www/html
RUN git clone https://gitlab.developers.cam.ac.uk/ch/co/project-light-minimal.git
WORKDIR /var/www/html/project-light-minimal
RUN git archive --format=tar.gz -o /tmp/pl.tgz HEAD
RUN tar -C /var/www/html -x -f /tmp/pl.tgz

# set up FastAPI app
WORKDIR /var/www/code
ENV VIRTUAL_ENV=/var/www/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY requirements.txt /var/www/code
RUN pip install --no-cache-dir --upgrade -r requirements.txt
RUN mkdir /var/lib/mifare

CMD ./go.sh
