#!/bin/bash
set -e

cd /var/www/code
. /var/www/venv/bin/activate
DATABASE_HOST=dbdev.ch.private.cam.ac.uk uvicorn main:app --reload --host 0.0.0.0
