import csv
import os
import string
import tempfile
import zipfile
from io import BytesIO, StringIO

import psycopg2
import psycopg2.extras
from fastapi import FastAPI, File, Form, Request, UploadFile
from fastapi.responses import FileResponse, HTMLResponse, Response
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from psycopg2 import sql
from typing_extensions import Annotated

app = FastAPI(root_path=os.environ.get("ROOT_PATH", ""), openapi_url="")

app.mount("/pl", StaticFiles(directory="pl"), name="project_light")

templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/mifare_from_crsid", response_class=HTMLResponse)
async def mifare_from_crsid_form(request: Request):
    return templates.TemplateResponse("mifare_from_crsid.html", {"request": request})


def get_db_conn(username, password):
    host = os.environ.get("DATABASE_HOST", "database.ch.private.cam.ac.uk")
    dbname = os.environ.get("DATABASE_NAME", "chemistry")
    conn_string = f"postgresql://{username}:{password}@{host}/{dbname}"
    return psycopg2.connect(conn_string)


@app.post("/mifare_from_crsid", response_class=HTMLResponse)
async def mifare_from_crsid(
    request: Request,
    username: Annotated[str, Form()],
    password: Annotated[str, Form()],
    csvfile: Annotated[UploadFile, File()],
):

    try:
        conn = get_db_conn(username, password)
    except psycopg2.OperationalError as e:
        db_conn_error = (
            "ERROR: Could not connect to the database. ERROR MESSAGE: " + str(e)
        )
        return templates.TemplateResponse(
            "mifare_from_crsid.html", {"request": request, "error": db_conn_error}
        )

    if not csvfile.filename.endswith(".csv"):
        file_format_error = "ERROR: Your input file must end with .csv"
        return templates.TemplateResponse(
            "mifare_from_crsid.html", {"request": request, "error": file_format_error}
        )

    with tempfile.NamedTemporaryFile(delete=False) as mifare_infile:
        mifare_infile.write(await csvfile.read())
        mifare_infile.flush()
        mifare_infile.seek(0)

    with open(mifare_infile.name, "r") as inf:
        reader = csv.DictReader(inf)
        reader.__next__()
        fields = reader.fieldnames
        if "Card Number" not in fields:
            fields.append("Card Number")

    mifare_outfile_name = csvfile.filename.replace(".csv", "-with-mifare.csv")

    with open(mifare_infile.name, "r") as inf, tempfile.NamedTemporaryFile(
        "w", delete=False
    ) as outf:
        reader = csv.DictReader(inf)
        writer = csv.DictWriter(outf, fieldnames=fields)
        writer.writeheader()
        try:
            for line in reader:
                crsid = line["CRSID"]
                cursor = conn.cursor()
                cursor.execute(
                    """SELECT
                           mifare_number
                       FROM
                           university_card
                        WHERE crsid = lower(%s)
                        ORDER BY expires_at DESC
                        LIMIT 1""",
                    [crsid],
                )
                if cursor.rowcount == 1:
                    line["Card Number"] = cursor.fetchone()[0]
                writer.writerow(line)
        except (KeyError, ValueError):
            field_data_error = (
                "ERROR: Check that your input file contains the 'CRSID' column."
            )
            return templates.TemplateResponse(
                "mifare_from_crsid.html",
                {"request": request, "error": field_data_error},
            )
            conn.close()
    conn.close()

    os.remove(mifare_infile.name)

    return FileResponse(outf.name, filename=mifare_outfile_name)


@app.get("/signin_system_csv", response_class=HTMLResponse)
async def signin_system_csvs_form(request: Request):
    return templates.TemplateResponse("signin_system_csvs.html", {"request": request})


@app.post("/signin_system_csv", response_class=HTMLResponse)
async def signin_system_csvs(
    request: Request,
    username: Annotated[str, Form()],
    password: Annotated[str, Form()],
):
    database_columns = [
        "Name",
        "Email",
        "Mobile",
        "Role",
        "RFID",
    ]
    step_size = 2
    chunk_matches = [
        string.ascii_lowercase[start : start + step_size]
        for start in range(0, len(string.ascii_lowercase), step_size)
    ] + [string.digits]

    max_chunk_size = 10000  # fail if groups get bigger than this
    database_names = {
        "columns": sql.SQL(", ").join([sql.Identifier(c) for c in database_columns]),
        "schema": sql.Identifier("apps"),
        "view": sql.Identifier("signin_system_grouped_cards"),
    }

    def group_size_ok(count, chunk_size):
        return count <= chunk_size

    def create_in_memory_csv(rows):
        f = StringIO()
        writer = csv.writer(f)
        writer.writerows(rows)
        return f

    try:
        conn = get_db_conn(username, password)
    except psycopg2.OperationalError as e:
        db_conn_error = "Could not connect to the database: " + str(e)
        return templates.TemplateResponse(
            "error.html", {"request": request, "error": db_conn_error}
        )
    zip_file = BytesIO()
    with conn.cursor() as c, zipfile.ZipFile(zip_file, mode="w") as z:
        # get people in special groups
        c.execute(
            sql.SQL(
                "SELECT DISTINCT group_name FROM {schema}.{view} WHERE group_name IS NOT NULL"
            ).format(**database_names)
        )
        groups = [row[0] for row in c.fetchall()]
        for group in groups:
            c.execute(
                sql.SQL(
                    """SELECT {columns}
                       FROM {schema}.{view}
                       WHERE group_name = %s
                       ORDER BY "Email" """
                ).format(**database_names),
                [group],
            )
            column_names = [desc[0] for desc in c.description]
            if not group_size_ok(c.rowcount, max_chunk_size):
                return templates.TemplateResponse(
                    "error.html",
                    {
                        "request": request,
                        "error": f"Group {group} has more than {max_chunk_size} members",
                    },
                )
            f = create_in_memory_csv([column_names] + c.fetchall())
            z.writestr(f"{group}.csv", f.getvalue())
        # Get the ungrouped records, splitting into predictable chunks
        for characters in chunk_matches:
            section_name = f"Others-emails-starting-{characters}"
            re_match = f"^[{characters}]"
            c.execute(
                sql.SQL(
                    """
                SELECT {columns}
                FROM {schema}.{view}
                WHERE group_name IS NULL
                AND
                "Email" ~* %s
                ORDER BY "Email" """
                ).format(**database_names),
                [re_match],
            )
            if not group_size_ok(c.rowcount, max_chunk_size):
                return templates.TemplateResponse(
                    "error.html",
                    {
                        "request": request,
                        "error": f"Group {section_name} has more than {max_chunk_size} members",
                    },
                )
            column_names = [desc[0] for desc in c.description]
            if c.rowcount > 0:
                f = create_in_memory_csv([column_names] + c.fetchall())
                z.writestr(f"{section_name}.csv", f.getvalue())

    return Response(content=zip_file.getvalue(), media_type="application/zip")
